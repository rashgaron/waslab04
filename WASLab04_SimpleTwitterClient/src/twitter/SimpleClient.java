package twitter;

import java.util.Date;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

public class SimpleClient {

	public static void main(String[] args) throws Exception {
		
		final Twitter twitter = new TwitterFactory().getInstance();
		
		TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
		twitterStream.addListener(getTwitterStream());
		
		FilterQuery tweetFilterQuery = new FilterQuery();
		tweetFilterQuery.track("#covid19");
		
		twitterStream.filter(tweetFilterQuery);
				
	}
	
	
	
	
	
	
	private static StatusListener getTwitterStream() {
		
		return new StatusListener() {

			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onScrubGeo(long arg0, long arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStallWarning(StallWarning arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStatus(Status st) {
				// TODO Auto-generated method stub
				twitter4j.User u = st.getUser();
				System.out.println(
						u.getName() + "(@" +  u.getScreenName() + "): "
						+ "RT @" + st.getRetweetedStatus().getUser().getScreenName() +": "+ st.getText());
			}

			@Override
			public void onTrackLimitationNotice(int arg0) {
				// TODO Auto-generated method stub
				
			}			
			
		};
		
		
	}
}
